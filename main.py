import dataclasses
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import minmax_scale
import torch
import torch.nn as nn


@dataclasses.dataclass
class Parameters:
    nrows: int = 512
    ncols: int = 512
    imgmul: int = 255
    nlayers: int = 8
    hsize: int = 32


class CPPN(nn.Module):
    def __init__(self, inp_dim=3, hid_dim=64, n_layers=8, activation='tanh'):
        super(CPPN, self).__init__()

        self.activations = {
            'sigmoid': nn.Sigmoid(),
            'tanh': nn.Tanh(),
            'relu': nn.ReLU(),
            'shrink': nn.Hardshrink()
        }

        self.n_layers = n_layers
        self.input_layer = nn.Linear(inp_dim, hid_dim)
        self.layer = nn.Linear(hid_dim, hid_dim)
        self.out_layer = nn.Linear(hid_dim, 3)
        self.activation = self.activations[activation]

    def forward(self, inputs):
        x = self.input_layer(inputs)
        x = self.activation(x)
        for i in range(self.n_layers-2):
            x = self.layer(x)
            x = self.activation(x)
        x = self.out_layer(x)
        x = self.activation(x)
        return x


def build_cpnn(nlayers, hsize):
    cppn = []
    for i in range(0, nlayers):
        if i == 0:
            mutator = np.random.randn(3, hsize)
        elif i == nlayers - 1:
            mutator = np.random.randn(hsize, 3)
        else:
            mutator = np.random.randn(hsize, hsize)
        mutator = mutator.astype(np.float32)
        cppn.append(mutator)

    return cppn


def init_weights(m):
    """Init random weight initialization"""
    if isinstance(m, nn.Linear):
        torch.nn.init.normal_(m.weight)
        m.bias.data.fill_(0.01)


rowmat = (np.tile(np.linspace(0, Parameters.nrows-1, Parameters.nrows, dtype=np.float32), Parameters.ncols)
          .reshape(Parameters.ncols, Parameters.nrows).T - Parameters.nrows / 2.0) / (Parameters.nrows / 2.0)
colmat = (np.tile(np.linspace(0, Parameters.ncols-1, Parameters.ncols, dtype=np.float32), Parameters.nrows)
          .reshape(Parameters.nrows, Parameters.ncols) - Parameters.ncols / 2.0) / (Parameters.ncols / 2.0)
inputs = np.stack([rowmat, colmat, np.sqrt(np.power(rowmat, 2) + np.power(colmat, 2))])\
    .transpose(1, 2, 0)

grid = inputs.reshape(-1, 3).astype(np.float32)

img = minmax_scale(grid)
img *= Parameters.imgmul
img = img.reshape(Parameters.nrows, Parameters.ncols, 3).astype(np.int32)
plt.figure(figsize=(10, 10))
plt.imshow(img)

cppn = build_cpnn(Parameters.nlayers, Parameters.hsize)
gen_img = grid.copy()

for layer in cppn:
    gen_img = np.tanh(np.matmul(gen_img, layer))

gen_img = minmax_scale(gen_img)
gen_img *= 255.
gen_img = gen_img.reshape(Parameters.nrows, Parameters.ncols, 3).astype(np.int32)
plt.figure(figsize=(10, 10))
plt.imshow(gen_img)

model = CPPN(activation='tanh')
model.apply(init_weights)
tensor_grid = torch.tensor(grid)
output = model(tensor_grid).detach().numpy()

plt.show()

